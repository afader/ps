function wordWrap(str, maxWidth) {
    if (!str) return ''
    var newLineStr = "\n"; done = false; res = '';
    while (str.length > maxWidth) {                 
        found = false;
        // Inserts new line at first whitespace of the line
        for (i = maxWidth - 1; i >= 0; i--) {
            if (testWhite(str.charAt(i))) {
                res = res + [str.slice(0, i), newLineStr].join('');
                str = str.slice(i + 1);
                found = true;
                break;
            }
        }
        // Inserts new line at maxWidth position, the word is too long to wrap
        if (!found) {
            res += [str.slice(0, maxWidth), newLineStr].join('');
            str = str.slice(maxWidth);
        }

    }

    return res + str;
}
function testWhite(x) {
    var white = new RegExp(/^\s$/);
    return white.test(x.charAt(0));
};

function setInfo(note) {
  const title = document.getElementById('title')
  title && (title.innerText = note.Title)
  const descr = document.getElementById('description')
  descr && (descr.innerText = note.Text)
  document.getElementById('zettel-link').href = 'https://www.appsheet.com/start/' + window.appId + '#control=Notes_Detail&page=detail&row='+note.Key
  document.getElementById('center-link').href = note.Key + '.html'
  document.getElementById('content').style.display = 'block'
}

function clearInfo() {
  document.getElementById('content').style.display = 'none'
}

function render(data) {
  var center = data.center
  var centerNote = data.notes.find(x => x.Key === center)
  setInfo(centerNote)

  var seen = {}
  data.links = data.links.filter(x => {
    if (seen[x.From + x.To]) return false
    seen[x.From + x.To] = 1
    seen[x.To + x.From] = 1
    return true
  })

  var id2index = {}
  var nodes = data.notes.map((x, i) => {
    id2index[x.Key] = i
    return {
      id: x.Key,
      label: wordWrap(x.Title, 20),
      shape: 'box',
      group: x.Cluster,
    }
  })

  var edges = data.links.map(x => {
    return {
      from: x.From,
      to: x.To,
    }
  })

  var links = []
  var seen = {}
  data.links.forEach(x => {
    if (x.From === center || x.To === center) return
    if (seen[x.From+' '+x.To]) return
    links.push({
      source: id2index[x.From],
      target: id2index[x.To],
    })
    seen[x.From+' '+x.To] = true
    seen[x.To+' ' + x.From] = true
  })

  // create an array with nodes
  var nodesData = new vis.DataSet(nodes.filter(x => x.id !== center));

  // create an array with edges
  var edgesData = new vis.DataSet(edges.filter(x => x.from !== center && x.to !== center));

  // create a network
  var container = document.getElementById('mynetwork');
  var netdata = {
    nodes: nodesData,
    edges: edgesData
  };
  var options = {height: '100%',physics:{enabled:true,stabilization:{iterations:100}}};
  var network = new vis.Network(container, netdata, options);
  network.on('click', (props) => props.nodes.length && setInfo(data.notes.find(x => x.Key === props.nodes[0])))
  network.on('click', (props) => !props.nodes.length && setInfo(centerNote))
  network.on('doubleClick', (props) => props.nodes.length && (window.location.href = props.nodes[0] + '.html'))
}

function main(data) {
  window.appId = data.appId
  render(data.shard)
}